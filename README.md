# Linear Solver

This project provides a ASP.Net Web API to solve combinatorial optimization problems, in detail [linear problems](https://de.wikipedia.org/wiki/Lineare_Optimierung).

## Linear Problem Example
Imagine you can produce two goods x1 and x2 and you earn 10$ for selling x1 and 20$ for x2. Additionaly there are 3 ressource types (a,b,c) with 100, 720, 60 units on stock that are needed to produce x1 and/or x2. The production of x1 takes you 1 unit of ressource a and 6 units b. The production of x2 needs 1 unit of a, 9 of b and 1 of ressource c.

The question is: How many units of x1 and x2 should we produce to maximise our revenue?

**This linear problem can be stated as:**

max 10x1 + 20x2

under following constraints:

x1 + x2 <= 100

6x1 + 9x2 <=720

x2 <= 60

## API functionality
**Problem Solving**

This web api implements the [simplex algorithm](https://en.wikipedia.org/wiki/Simplex_algorithm) to solve linear problems. This method was originally presented by George Dantzig.

**State of Development**
- Currently only non-negative coefficients for the objective function and the constraint parameters are allowed. 
- Only maximization problems are considered.
- coefficients with value 1 must be explicitly written so (eg 1x2)
- <= constraints must be written as "=", this is due to constraint string validation only and might be altered in future versions

**Route: /LinearProgram (POST)**

Request Data:
- "objectiveFunction": string
- "constraints": List of strings representing constraints
```json
{
  "objectiveFunction": "10x1 + 20x2",
  "constraints": [
    "1x1 + 1x2 = 100", 
    "6x1 + 9x2 = 720", 
    "1x2 = 60"
  ]
}
```

Answer (response body):
```json
{
  "x1": 30,
  "x2": 60,
  "Objective Value": 1500
}
```


## Project Structure
TODO

## Planned Features
- refactor datastructures and dependencies
- provide support to more type of combinatorial problems and solving algorithms like:
    - mixed integer linear problems
    - branch and bound / branch and cut methods
    - ...
