﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Tests
{
    [TestClass()]
    public class LinearModelTests
    {
        double SolutionPrecision = 0.00001;

        [TestMethod()]
        public void Test_ModelPrimalSimple_Correct()
        {
            // Define Input Data
            double[,] m = new double[,] { { 1, 1, 1, 0, 0, 100 }, { 6, 9, 0, 1, 0, 720 }, { 0, 1, 0, 0, 1, 60 }, { -10, -20, 0, 0, 0, 0 } };
            var basic_variables = new List<string>(){ "slope_0", "slope_1", "slope_2" };
            var free_variables = new List<string> { "x1", "x2" };

            // Create Model and solve Linear
            var model = new LinearModel(m, basic_variables, free_variables);
            Dictionary<string,object> solution = model.SolveLinear();

            // Compare outcomes
            Assert.AreEqual(solution["x1"], (double) 30);
            Assert.AreEqual(solution["x2"], (double) 60);
            Assert.AreEqual(solution["Objective Value"], (double) 1500);

        }

        [TestMethod()]
        public void Test_ModelDualSimple_Correct()
        {
            // Define Input Data
            double[,] m = new double[,] { { -6, -5, 1, 0, 0, -120 }, { 3, 5, 0, 1, 0, 110 }, { -2, -4, 0, 0, 1, -80 }, { -4, -5, 0, 0, 0, 0 } };
            var basic_variables = new List<string>() { "slack_0", "slack_1", "slack_2" };
            var free_variables = new List<string>() { "x1", "x2" };

            // Create Model and solve Linear
            var model = new LinearModel(m, basic_variables, free_variables);
            Dictionary<string, object> solution = model.SolveLinear();

            // Compare outcomes
            Assert.IsTrue(Math.Abs((double) solution["x1"] - (double) 20) < SolutionPrecision);
            Assert.IsTrue(Math.Abs((double) solution["x2"] - (double) 10) < SolutionPrecision);
            Assert.IsTrue(Math.Abs((double)solution["Objective Value"] - (double)130) < SolutionPrecision);
        }
    }
}