﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using InputModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolverExceptions;

namespace InputModels.Tests
{
    [TestClass()]
    public class LinearModelInputTests
    {
        /// <summary>
        /// General, most simple and correct input
        /// </summary>
        [TestMethod()]
        public void Test_CreateTableauMatrix_ValidInput()
        {
            // Define Input
            string objectiveFunction = "max 10x1 + 20x2";
            string[] constraints = new string[] { "1x1 + 1x2 = 100", "6x1 + 9x2 = 720", "1x2 = 60" };

            LinearModelInput model = new LinearModelInput(objectiveFunction, constraints);

            Tuple<double[,], List<string>, List<string>> output = model.CreateTableauMatrixData();

            // Define Target Output
            double[,] target_m = new double[,] { { 1, 1, 1, 0, 0, 100 }, { 6, 9 , 0, 1, 0, 720 }, { 0, 1, 0, 0, 1, 60}, { -10, -20 , 0, 0, 0, 0} };
            string[] target_bv = new string[] { "slope_0", "slope_1", "slope_2" };
            string[] target_nbv = new string[] { "x1", "x2" };

            CollectionAssert.AreEqual(target_m, output.Item1);
            CollectionAssert.AreEqual(target_bv, output.Item3);
            CollectionAssert.AreEqual(target_nbv, output.Item2);
        }

        /// <summary>
        /// Test Transformation of Min Objective Function to Max Objective Function
        /// </summary>
        [TestMethod()]
        public void Test_CreateTableauMatrix_TransformMinObjectiveFunction()
        {
            // Define Input
            string objectiveFunction = "min -10x1 -20x2";
            string[] constraints = new string[] { "1x1 + 1x2 = 100", "6x1 + 9x2 = 720", "1x2 = 60" };

            LinearModelInput model = new LinearModelInput(objectiveFunction, constraints);

            Tuple<double[,], List<string>, List<string>> output = model.CreateTableauMatrixData();

            // Define Target Output
            double[,] target_m = new double[,] { { 1, 1, 1, 0, 0, 100 }, { 6, 9, 0, 1, 0, 720 }, { 0, 1, 0, 0, 1, 60 }, { -10, -20, 0, 0, 0, 0 } };
            string[] target_bv = new string[] { "slope_0", "slope_1", "slope_2" };
            string[] target_nbv = new string[] { "x1", "x2" };

            CollectionAssert.AreEqual(target_m, output.Item1);
            CollectionAssert.AreEqual(target_bv, output.Item3);
            CollectionAssert.AreEqual(target_nbv, output.Item2);
        }

        /// <summary>
        /// Test correct processing of "x1" instead of "1x1" within objective function and constraints
        /// </summary>
        [TestMethod]
        public void Test_CreateTableauMatrix_OmitOnes()
        {
            // Define Input
            string objectiveFunction = "max 10x1 + x2";
            string[] constraints = new string[] { "x1 + x2 = 100", "6x1 + 9x2 = 720", "x2 = 60" };

            LinearModelInput model = new LinearModelInput(objectiveFunction, constraints);

            Tuple < double[,], List<string>, List<string>> output = model.CreateTableauMatrixData();

            // Define Target Output
            double[,] target_m = new double[,] { { 1, 1, 1, 0, 0, 100 }, { 6, 9, 0, 1, 0, 720 }, { 0, 1, 0, 0, 1, 60 }, { -10, -1, 0, 0, 0, 0 } };
            string[] target_bv = new string[] { "slope_0", "slope_1", "slope_2" };
            string[] target_nbv = new string[] { "x1", "x2" };

            CollectionAssert.AreEqual(target_m, output.Item1);
            CollectionAssert.AreEqual(target_bv, output.Item3);
            CollectionAssert.AreEqual(target_nbv, output.Item2);
        }

        /// <summary>
        /// Test for Missing or wrong objective function specifier ("min" or "max")
        /// </summary>
        [TestMethod]
        public void Test_FaultyObjectiveFunction_Exception()
        {
            // Define Input
            string objectiveFunction = "foo 10x1 + 20x2";
            string objectiveFunctionVar = "10x1 + 20x2";
            string[] constraints = new string[] { "1x1 + 1x2 = 100", "6x1 + 9x2 = 720", "1x2 = 60" };

            LinearModelInput model = new LinearModelInput(objectiveFunction, constraints);
            Assert.ThrowsException<InvalidModelInputException>(() => model.CreateTableauMatrixData());
            model = new LinearModelInput(objectiveFunctionVar, constraints);
            Assert.ThrowsException<InvalidModelInputException>(() => model.CreateTableauMatrixData());
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Test_ParseCoefficient()
        {
            string factor = "1";
            var output = LinearModelInput.ParseCoefficient(factor);
            Assert.AreEqual(output, (double)1);

            factor = " + ";
            output = LinearModelInput.ParseCoefficient(factor);
            Assert.AreEqual(output, (double)1);

            factor = " - ";
            output = LinearModelInput.ParseCoefficient(factor);
            Assert.AreEqual(output, (double)-1);

        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Test_ConstraintParsing()
        {
            string constraint = "2x1 + y - 3z";
            var output = LinearModelInput.ParseConstraint(constraint);
            Assert.AreEqual("2x1", output[0]);
            Assert.AreEqual("+ y", output[1]);
            Assert.AreEqual("- 3z", output[2]);

            constraint = " 10x1 + 20x2 ";
            output = LinearModelInput.ParseConstraint(constraint);
            Assert.AreEqual("10x1", output[0]);
            Assert.AreEqual("+ 20x2", output[1]);
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Test_FactorParsing()
        {
            string factor = "2x1";
            var output = LinearModelInput.ParseFactor(factor);
            Assert.AreEqual(output.Item1, "2");
            Assert.AreEqual(output.Item2, "x1");

            factor = " zzz ";
            output = LinearModelInput.ParseFactor(factor);
            Assert.AreEqual(output.Item1, "");
            Assert.AreEqual(output.Item2, "zzz");
        }
    }
}