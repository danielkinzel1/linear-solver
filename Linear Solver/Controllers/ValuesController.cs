﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using InputModels;
using Linear_Solver;
using SolverExceptions;
using Models;

namespace Linear_Solver.Controllers
{
    [Route("LinearProgram")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [HttpPost]
        public IActionResult SolveLinearProblem(LinearModelInput model)
        {
            try
            {
                LinearModel LinSolver = new(model);
                return Ok(LinSolver.SolveLinear());
            } catch (InvalidModelException ex)
            {
                return BadRequest(ex.Message);
            } catch (InvalidModelInputException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
