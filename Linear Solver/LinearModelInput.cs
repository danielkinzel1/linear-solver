using SolverExceptions;
using System.Data.Common;
using System.Linq.Expressions;
using System.Numerics;

namespace InputModels
{
    public class LinearModelInput
    {
        // Objective Function (for example "2a + 3b")
        public string ObjectiveFunction { get; set; }
        // List of Constraints (for example ["a + 2b = 10", "3a + 4b = 25"])
        public string[] Constraints { get; set; }

        public LinearModelInput(string objectiveFunction, string[] constraints)
        {
            ObjectiveFunction = objectiveFunction.Trim();
            Constraints = constraints;
        }


        public Tuple<double[,], List<string>, List<string>> CreateTableauMatrixData()
        {

            // TODO Implement more split character options
            List<string> DecisionVariables = new List<string>();
            List<string> ArtificialVariables = new List<string>();
            List<double> MultiplicationFactors = new List<double>();

            int factorMaxObjective;

            // Parse Objective Function and transform to max formulation
            if (ObjectiveFunction.StartsWith("max")) 
            {
                factorMaxObjective = 1;
            } else if (ObjectiveFunction.StartsWith("min"))
            {
                factorMaxObjective = -1;
            } else
            {
                throw new InvalidModelInputException($"Cannot process ObjectiveFunction: {ObjectiveFunction}. Specify 'min' or 'max'.");
            }
            var objectiveFactors = ParseConstraint(ObjectiveFunction.Substring(3).Trim());

            // Iterate through all Objective Function Factors and extract Decision Variables and Multiplication Factors
            foreach (string factorObj in objectiveFactors)
            {
                (string Value, string Identifier) factor = ParseFactor(factorObj.Trim());
                DecisionVariables.Add(factor.Identifier);
                double _fact = ParseCoefficient(factor.Value);
                MultiplicationFactors.Add(-_fact * factorMaxObjective);
            }

            // Declare Tableau Matrix
            int N_Constraints = Constraints.Length;
            double[,] m = new double[N_Constraints + 1, N_Constraints + DecisionVariables.Count + 1];
            int max_j_m = N_Constraints + DecisionVariables.Count;
            int max_i_m = N_Constraints;

            // Iterate over Constraints and populate Tableau Matrix
            for (int i = 0; i < N_Constraints; i++)
            {
                // TODO Implement more separator split options
                var _temp = Constraints[i].Split('=');
                // TODO Implement a suitable Exception handling and own Exception Class for this project
                if (_temp.Length != 2) { throw new InvalidModelInputException($"Error at handling Constraint: '{Constraints[i]}'"); }
                // Fill "b" Value of line
                m[i, max_j_m] = int.Parse(_temp[1]);

                // Fill coefficients for decision Variables
                List<string> nbFactors = ParseConstraint(_temp[0]);
                foreach (var f in nbFactors)
                {
                    (string Value, string identifier) factor = ParseFactor(f);

                    double _factor_value = ParseCoefficient(factor.Value);
                    m[i, DecisionVariables.IndexOf(factor.identifier)] = _factor_value;
                }
                // Fill column values for artificial Variable
                m[i, i + DecisionVariables.Count] = 1;
                ArtificialVariables.Add($"slope_{i}");
            }

            // Enter Objective Function Values into Tableau Matrix
            for (int i = 0; i < MultiplicationFactors.Count; i++)
            {
                m[max_i_m, i] = MultiplicationFactors[i];
            }
            var Return_Tuple = new Tuple<double[,], List<string>, List<string>>(m, DecisionVariables, ArtificialVariables);
            return Return_Tuple;
        }

        /// <summary>
        /// Parses a Coefficient Value consisting out of a string to a double value. For Example "1" to 1.0, "- 3" to -3.0, "-" to -1.0
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static double ParseCoefficient(string str)
        {
            str = str.Trim();
            if (str.Equals("") || str.Equals("+"))
            {
                return 1;
            }
            else if (str.Equals("-"))
            {
                return -1;
            }
            else
            {
                return Double.Parse(str);
            }
        }

        /// <summary>
        /// Parse a factor to its Components: a Coefficient value and the related Decision Variable
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static (string, string) ParseFactor(string str)
        {
            int pointer = 0;
            var factor = str.Trim();
            char[] allowedFactorChars = new char[] { '-', '.', '+', ' ' };

            // Find split character for factor and related Desicion Variable
            while (Char.IsDigit(factor[pointer]) || allowedFactorChars.Contains(factor[pointer]))
            {
                pointer++;
            }
            var factorValueString = factor.Substring(0, pointer).Trim().Replace(" ", "");
            var variableIdentifier = factor.Substring(pointer).Trim();

            return (factorValueString, variableIdentifier);
        }

        /// <summary>
        /// Parses a String describing a Constraint to a List of Factors.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static List<string> ParseConstraint(string str)
        {
            List<string> nbFactors = new List<string> { };
            int pointerStart = 0;
            int pointerEnd = 1;

            while (pointerEnd < str.Length - 1)
            {
                // Find next factor separating character
                while ((pointerEnd < str.Length) && !Char.Equals(str[pointerEnd], '+') &&
                    !Char.Equals(str[pointerEnd], '-'))
                {
                    pointerEnd++;
                }
                // Add substring (factor) to List of factors
                nbFactors.Add(str.Substring(pointerStart, pointerEnd - pointerStart).Trim());
                pointerStart = pointerEnd;
                pointerEnd++;

            }
            return nbFactors;
        }
    }
}