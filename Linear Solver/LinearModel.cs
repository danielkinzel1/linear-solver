using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Net.NetworkInformation;
using InputModels;
using SolverExceptions;

namespace Models
{
    public class LinearModel
    {
        public double[,] TableauMatrix { get; }
        public List<string> BaseVariables { get; }
        public List<string> NotBaseVariables { get; }
        public List<string> DecisionVariables { get; }
        public Dictionary<int, string> ColumnVariableLookup { get; }


        public LinearModel(double[,] m, List<string> BasicVars, List<string> FreeVars)
        {
            TableauMatrix = m;
            BaseVariables = BasicVars;
            NotBaseVariables = FreeVars;
            DecisionVariables = new List<string> (NotBaseVariables);
            ColumnVariableLookup = new Dictionary<int, string>() { };

            // Create Dictionary that holds information which column represents which variable
            List<string> AllVariables = DecisionVariables.Concat(BaseVariables).ToList();
            for(int i = 0; i < AllVariables.Count; i++)
            {
                ColumnVariableLookup.Add(i, AllVariables[i]);
            }
        }

        public LinearModel(LinearModelInput model) {
            // TODO: Refactor this dependency structure
            var model_input = model.CreateTableauMatrixData();
            TableauMatrix = model_input.Item1;
            BaseVariables = model_input.Item3;
            NotBaseVariables = model_input.Item2;
            DecisionVariables = new List<string>(NotBaseVariables);
            ColumnVariableLookup = new Dictionary<int, string>() { };

            // Create Dictionary that holds information which column represents which variable
            var AllVariables = DecisionVariables.Concat(BaseVariables).ToList();
            for (int i = 0; i < AllVariables.Count; i++)
            {
                ColumnVariableLookup.Add(i, AllVariables[i]);
            }
        }

        /// <summary>
        /// Solve Linear Model. Return Dictionary with decision Variables Values and objective Function Value.
        /// </summary>
        /// <returns>Dictionary with Keys for Decision Variables and Objective Function Value</returns>
        /// <exception cref="Exception"></exception>
        public Dictionary<string, object> SolveLinear()
        {
            int jMax = TableauMatrix.GetLength(1) - 1;
            int iMax = TableauMatrix.GetLength(0) - 1;

            List<string> SolutionVars = new List<string>(NotBaseVariables);
            
            // Check if negative restriction values exist --> Perform dual Simplex Method
            if (CheckNegativeConstraints(TableauMatrix, jMax))
            {
                PerformDualSimplex();
            }

            // Perform Primal Simplex Method
            while (CheckFRow(TableauMatrix))
            {
                PerformPrimalSimplex();
            }

            // Construct Return Value
            Dictionary<string, object> ResultDict = new Dictionary<string, object>();
            for(int i = 0; i < DecisionVariables.Count; i++)
            {
                ResultDict.Add(DecisionVariables[i], 0);
            }
            for(int i = 0; i < BaseVariables.Count; i++)
            {
                if (DecisionVariables.Contains(BaseVariables[i]))
                {
                    ResultDict[BaseVariables[i]] = TableauMatrix[i, jMax];
                }
            }
            ResultDict.Add("Objective Value", TableauMatrix[iMax, jMax]);
            return ResultDict;
        }

        private void PerformPrimalSimplex()
        {
            int jMax = TableauMatrix.GetLength(1) - 1;
            int iMax = TableauMatrix.GetLength(0) - 1;

            // Perform Pivot Transformation
            // Identify Pivot Element
            int j_p = GetMinIndexRow(TableauMatrix, iMax);
            int i_p = GetMinIndexCol(TableauMatrix, j_p);

            // Check for unconstrained Problem
            if (TableauMatrix[i_p, j_p] < 0)
            {
                throw new InvalidModelException("Problem not restricted");
            }

            // Update Basic Variables and Free Variable labels
            int _temp_idx = NotBaseVariables.IndexOf(ColumnVariableLookup[j_p]);
            string _temp_basevar = BaseVariables[i_p];
            BaseVariables[i_p] = ColumnVariableLookup[j_p];
            NotBaseVariables[_temp_idx] = _temp_basevar;

            // Update Pivot Line, including B Column
            double pivot_value = TableauMatrix[i_p, j_p];
            for (int j = 0; j <= jMax; j++)
            {
                TableauMatrix[i_p, j] = TableauMatrix[i_p, j] * (1 / pivot_value);
            }

            // Update other rows, including F row
            for (int i = 0; i <= iMax; i++)
            {
                if (i != i_p)
                {
                    double _column_factor = TableauMatrix[i, j_p];
                    for (int j = 0; j <= jMax; j++)
                    {
                        TableauMatrix[i, j] = TableauMatrix[i, j] - (TableauMatrix[i_p, j] * _column_factor);
                    }
                }
            }
        }

        /// <summary>
        /// Perform Dual Simplex Method on TableauMatrix
        /// </summary>
        private void PerformDualSimplex()
        {
            int jMax = TableauMatrix.GetLength(1) - 1;
            while (CheckNegativeConstraints(TableauMatrix, jMax))
            {
                // Identify Pivot Column
                int i_p = GetMinIndexRowDual(TableauMatrix, jMax);
                // Identify Pivot Row
                int j_p = GetMaxIndexColumnDual(TableauMatrix, i_p);

                double pivotElem = TableauMatrix[i_p, j_p];

                // Perform Pivot Transformation
                int _temp_idx = NotBaseVariables.IndexOf(ColumnVariableLookup[j_p]);
                string _temp_basevar = BaseVariables[i_p];
                BaseVariables[i_p] = ColumnVariableLookup[j_p];
                NotBaseVariables[_temp_idx] = _temp_basevar;
                
                // Transform Pivot row
                for (int j = 0; j <= jMax; j++)
                {
                    TableauMatrix[i_p, j] = TableauMatrix[i_p, j] / pivotElem;
                }

                // Update other rows, including F row
                for (int i = 0; i < TableauMatrix.GetLength(0); i++)
                {
                    if (i != i_p)
                    {
                        double _column_factor = TableauMatrix[i, j_p];
                        for (int j = 0; j <= jMax; j++)
                        {
                            TableauMatrix[i, j] = TableauMatrix[i, j] - (TableauMatrix[i_p, j] * _column_factor);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Function to check F Row of Matrix for negative Elements
        /// </summary>
        /// <param name="Matrix"></param>
        /// <returns></returns>
        private static bool CheckFRow(double[,] Matrix)
        {
            int _idx_row = Matrix.GetLength(0) - 1;
            for (int i = 0; i < Matrix.GetLength(1); i++)
            {
                if (Matrix[_idx_row,i] < 0)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Function to get Pivot Element Column Index
        /// </summary>
        /// <param name="Matrix"></param>
        /// <returns></returns>
        private static int GetMinIndexRow(double[,] Matrix, int row)
        {

            double min_value = int.MaxValue;
            int idx_min_value = 0;

            // Iterate over n - 1 columns, as last column has different purpose
            for (int i = 0; i < Matrix.GetLength(1) - 1; i++)
            {
                if (Matrix[row, i] < min_value)
                {
                    min_value = Matrix[row, i];
                    idx_min_value = i;
                }
            }
            return idx_min_value;
        }

        /// <summary>
        /// Function to get Pivot Element Row Index
        /// </summary>
        /// <returns></returns>
        private static int GetMinIndexCol(double[,] Matrix, int column)
        {
            double min_value = int.MaxValue;
            int? idx_min_value = null;
            int jMaxVal = Matrix.GetLength(1) - 1;

            // Iterate over n - 1 rows, as last row is reserved for F Values
            for (int i = 0;  i < Matrix.GetLength(0) - 1; i++)
            {
                try
                {
                    double _quota = Matrix[i, jMaxVal] / Matrix[i, column];
                    if (_quota < min_value & Matrix[i,column] > 0)
                    {
                        min_value = _quota;
                        idx_min_value = i;
                    }
                } catch (DivideByZeroException){

                }
            }
            return idx_min_value ?? throw new Exception("Problem is not constrained");
        }

        /// <summary>
        /// Get Index of Minimal quotient of F Value and column value in row
        /// </summary>
        /// <param name="Matrix"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        /// <exception cref="InvalidModelException"></exception>
        private static int GetMaxIndexColumnDual(double[,] Matrix, int row)
        {
            double max_value = -int.MaxValue;
            int? idx_max_value = null;
            int iMax = Matrix.GetLength(0) - 1;

            for (int j = 0; j < Matrix.GetLength(1) - 1; j++)
            {
                if (Matrix[row, j] < 0 & Matrix[iMax, j] / Matrix[row, j] > max_value)
                {
                    max_value = Matrix[iMax, j] / Matrix[row, j];
                    idx_max_value = j;
                }
            }
            return idx_max_value ?? throw new InvalidModelException("");
        }

        /// <summary>
        /// Function to get row index of minimal negative Constraint Value 
        /// </summary>
        /// <param name="Matrix"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private static int GetMinIndexRowDual(double[,] Matrix, int column) 
        {
            double min_value = 0;
            int? idx_min_value = null;

            // Iterate over n - 1 columns, as last row is reserved for Objective Value
            for (int i = 0; i < Matrix.GetLength(0) - 1; i++)
            {
                if (Matrix[i, column] < min_value & Matrix[i, column] < 0)
                {
                    min_value = Matrix[i, column];
                    idx_min_value = i;
                }
            }
            return idx_min_value ?? throw new Exception("");
        }

        /// <summary>
        /// Function to check if a TableauMatrix contains negative negative constraint values.
        /// This indicates a dual simplex algorithm iteration might be needed.
        /// </summary>
        /// <param name="Matrix"></param>
        /// <returns></returns>
        private static bool CheckNegativeConstraints(double[,] Matrix, int colIndex)
        {
            for (int i = 0; i < Matrix.GetLength(0); i++)
            {
                if (Matrix[i, colIndex] < 0)
                {
                    return true;
                }
            }
            return false;
        }

    }

}