namespace SolverExceptions
{
    public class InvalidModelInputException : Exception
    {
        public InvalidModelInputException(string message) : base(message)
        {
            
        }
        
    }

    public class InvalidModelException: Exception
    {
        public InvalidModelException(string message) : base(message)
        {

        }
    }
}